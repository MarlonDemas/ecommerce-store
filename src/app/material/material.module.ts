import { NgModule } from '@angular/core';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatIconModule, MatIconRegistry } from '@angular/material/icon';
import { DomSanitizer } from '@angular/platform-browser';

const materialComponents = [MatInputModule, MatFormFieldModule, MatIconModule];
const svgIcons = [
  {
    name: 'search',
    path: '../../assets/search.svg',
  },
  {
    name: 'shopping_cart',
    path: '../../assets/shopping-cart.svg',
  },
  {
    name: 'profile_circle',
    path: '../../assets/profile-circle.svg',
  },
];

@NgModule({
  imports: [materialComponents],
  exports: [materialComponents],
})
export class MaterialModule {
  constructor(
    private matIconRegistry: MatIconRegistry,
    private domSanitizer: DomSanitizer
  ) {
    svgIcons.forEach((i) => {
      this.matIconRegistry.addSvgIcon(
        i.name,
        this.domSanitizer.bypassSecurityTrustResourceUrl(i.path)
      );
    });
  }
}
